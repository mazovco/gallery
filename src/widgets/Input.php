<?php

namespace mazovco\gallery\widgets;

use mazovco\gallery\assets\AssetBundle;
use mihaildev\elfinder\ElFinder;
use mihaildev\elfinder\AssetsCallBack;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

class Input extends \yii\widgets\InputWidget
{
    public $options = [];

    protected $_options = [
        'button' => [
            'id' => 'el_button',
            'class' => 'btn btn-success btn-add btn-md'
        ],
        'manager' => [
            'id' => 'el',
            'callback' => 'el',
            'language' => 'ru_RU',
            'filter' => 'image',
            'path' => 'path',
            'multiple' => 'true',
            'width' => 'auto',
            'height' => 'auto',
        ],
    ];

    public function init()
    {
        $this->options = ArrayHelper::merge($this->_options, $this->options);
        $this->options['manager']['url'] = ElFinder::getManagerUrl('elfinder', $this->options['manager']);
        parent::init();
    }

    public function run()
    {
        $view = $this->getView();
        AssetBundle::register($view);
        AssetsCallBack::register($view);
        //on click button open manager
        $view->registerJs("\$(document).on('click','#" . $this->options['button']['id'] . "', function(){mihaildev.elFinder.openManager(" . Json::encode($this->options['manager']) . ");});");
        return $this->render('input', ['options' => $this->options, 'model' => $this->model, 'attribute' => $this->attribute]);
    }
} 