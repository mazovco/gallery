<?php
/**
 * @var \yii\web\View $this
 * @var array $item
 * @var string $inputName
 */

use yii\helpers\Html;
?>

<div class="gallery-item" data-src="<?=$item['src']?>">
    <?= Html::textInput($inputName."[src]", $item['src'], ['class' => 'item-src']) ?>
    <?= Html::textInput($inputName."[label]", $item['label'], ['class' => 'item-label form-control']) ?>
    <?= Html::textInput($inputName. "[sort]", $item['sort'], ['class' => 'item-sort']) ?>
    <?= Html::img($item['src'], ['class' => 'item-img']) ?>
    <?= Html::a('', null, ['class' => 'btn btn-md btn-danger btn-flat fa fa-close item-remove']) ?>

</div>

