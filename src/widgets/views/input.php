<?php
/**
 * @var \yii\web\View $this
 * @var array $options
 * @var \yii\db\ActiveRecord $model
 * @var string $attribute
 */

use yii\helpers\Html;
?>

<div class="box gallery">
    <div class="box-header">
        <h5 class="box-title">Фоторепортаж</h5>
        <div class="box-tools pool-right"><?= Html::a('<i class="fa fa-camera"></i>&nbsp;Добавить', null, $options['button']) ?></div>
    </div>
    <div class="box-body">
        <div class="gallery-container">
            <?php if(!empty($model->{$attribute})): ?>
                <?php foreach ($model->{$attribute} as $i => $item ): ?>
                    <?= $this->render('_input_item', ['item' => $item, 'inputName' => Html::getInputName($model, $attribute."[$i]")]) ?>
                <?php endforeach ?>
            <?php endif ?>
        </div>
    </div>
</div>


