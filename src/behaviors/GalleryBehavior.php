<?php

namespace mazovco\gallery\behaviors;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

class GalleryBehavior extends \yii\base\Behavior
{
    /** @var  Аттрибут модели где храниться JSON галлереи */
    public $attribute;

    /** @var array [['src'=>'...', 'label'=>'...', 'sort'=>'1'], ..., []] */
    protected $_gallery = [];

    /**
     * @return array|mixed
     */
    public function getGallery()
    {
        if (empty($this->_gallery) && !empty($this->owner->{$this->attribute})) {
            $this->_gallery = Json::decode($this->owner->{$this->attribute});
        }
        return $this->_gallery;
    }

    /**
     * @param array|string $val
     */
    public function setGallery($val)
    {
        $this->_gallery = is_array($val) ? $val : Json::decode($val);
    }

    /**
     * @return array
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate'
        ];
    }


    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if (empty($this->_gallery)) {
            $this->owner->{$this->attribute} = '';
        } else {
            ArrayHelper::multisort($this->_gallery, 'sort');
            $this->owner->{$this->attribute} = Json::encode($this->_gallery);
        }
    }
}