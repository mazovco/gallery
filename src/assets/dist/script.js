$(function() {

    //Добавляем draggable сортировку изображениям
    var root = $('.gallery-container');
    root.sortable({
        //После перемещения элемента, изменяем input[name='sort'] у элементов
        'update': function (event, ui) {
            var sortIDs = root.sortable("toArray", {attribute : 'data-src'});
            for(var i in sortIDs) {
                $(".gallery-item").eq(i).children('input.item-sort').val(i);
            }
        }
    });

    // Удаление изображения по клику на кнопку remove
    $('.item-remove').on('click', function(){
        $(this).parent('.gallery-item').remove();
    });

    //Callback от elfinder. После выбора изображений, создаёт HTML DOM и добавляет его на страницу
    mihaildev.elFinder.register("el", function(files, id){
        var srclen = $('.item-src').length;
        for (var i in files) {
            _max = parseInt(srclen) + parseInt(i);
            console.log(_max);

            $('<div/>', {class : 'gallery-item ui-sortable-handle'}).attr('data-src', files[i].url)
                .append( $('<input/>', {class : 'item-src', type : 'text', value : files[i].url, name : 'News[gallery]['+ _max +'][src]'}) )
                .append( $('<input/>', {class : 'item-label', type : 'text', value : '', name : 'News[gallery]['+ _max +'][label]'}) )
                .append( $('<input/>', {class : 'item-sort', type : 'text', value : _max, name : 'News[gallery]['+ _max +'][sort]'}) )
                .append( $('<img/>', {class : 'item-img', src : files[i].url}) )
                .append( $('<a/>', {class: 'btn btn-md btn-danger btn-flat fa fa-close item-remove'}))
                .appendTo('.gallery-container');
        }
        return true;
    });
});
