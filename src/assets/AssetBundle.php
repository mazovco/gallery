<?php

namespace mazovco\gallery\assets;

class AssetBundle extends \yii\web\AssetBundle
{
    public $sourcePath = '@mazovco/gallery/assets/dist';

    public $js = [
        'script.js',
    ];

    public $css = [
        'styles.css'
    ];

    public $publishOptions = [
        'forceCopy' => YII_DEBUG,
    ];

    public $depends = [
        'yii\web\JqueryAsset',
        'yii\jui\JuiAsset',
        'mihaildev\elfinder\Assets',
        'mihaildev\elfinder\AssetsCallBack'

    ];
} 